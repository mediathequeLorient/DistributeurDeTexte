/* 
Copyright (C) 2018 Frédéric Micout / Médiathèque de Lorient

  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) toute version ultérieure.
Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

 */

#include <avr/pgmspace.h>

// Commentaire
//boolean etatBouton = LOW;
int compteur = 0;

// Texte à afficher systématiquement en entête 
const char entete[] = 
  "             Langue de bois\n"
  "Pour vous sortir de n'importe quelle\n"
  "situation imprévue !";
//"------------------------------------------";


// Textes à afficher
// Source : http://chrisferon.free.fr/technologies-langage/pipotron-discours-ena.php

// SECTION 1
const char texte_11[] PROGMEM = 
  "Mesdames, Messieurs,";
//"------------------------------------------"

const char texte_12[] PROGMEM = 
  "Je reste fondamentalement persuadé que";
//"------------------------------------------"

const char texte_13[] PROGMEM =  
  "Dès lors, sachez que je me battrai pour faire admettre que";
//"------------------------------------------" 

const char texte_14[] PROGMEM = 
  "Par ailleurs, c'est en toute connaissance de cause que je peux affirmer aujourd'hui que";
//"------------------------------------------"

const char texte_15[] PROGMEM = 
  "Je tiens à vous dire ici ma détermination sans faille pour clamer haut et fort que";
//"------------------------------------------"

const char texte_16[] PROGMEM = 
  "J'ai depuis longtemps (ai-je besoin de vous le rappeler ?) défendu l'idée que";
//"------------------------------------------"

const char texte_17[] PROGMEM =  
  "Et c'est en toute conscience que je déclare avec conviction que";
//"------------------------------------------" 

const char texte_18[] PROGMEM = 
  "Et ce n'est certainement pas vous, mes chers compatriotes, qui me contredirez...";
//"------------------------------------------"

const char* const section1[] PROGMEM = {texte_11, 
                                      texte_12, 
                                      texte_13, 
                                      texte_14, 
                                      texte_15,
                                      texte_16, 
                                      texte_17, 
                                      texte_18};

// SECTION 2
const char texte_21[] PROGMEM = 
  "la conjoncture actuelle";
//"------------------------------------------"

const char texte_22[] PROGMEM = 
  "la situation d'exclusion que certains d'entre vous connaissent";
//"------------------------------------------"

const char texte_23[] PROGMEM =  
  "l'acuité des problèmes de la vie quotidienne";
//"------------------------------------------" 

const char texte_24[] PROGMEM = 
  "la volonté farouche de sortir notre pays de la crise";
//"------------------------------------------"

const char texte_25[] PROGMEM = 
  "l'effort prioritaire en faveur du statut précaire des exclus";
//"------------------------------------------"

const char texte_26[] PROGMEM = 
  "le particularisme dû à notre histoire unique";
//"------------------------------------------"

const char texte_27[] PROGMEM =  
  "l'aspiration plus que légitime de chacun au progrès social";
//"------------------------------------------" 

const char texte_28[] PROGMEM = 
  "la nécessité de répondre à votre inquiétude journalière, que vous soyez jeunes...";
//"------------------------------------------"

const char* const section2[] PROGMEM = {texte_21, 
                                      texte_22, 
                                      texte_23, 
                                      texte_24, 
                                      texte_25,
                                      texte_26, 
                                      texte_27, 
                                      texte_28};

// SECTION 3
const char texte_31[] PROGMEM = 
  "doit s'intégrer à la finalisation globale";
//"------------------------------------------"

const char texte_32[] PROGMEM = 
  "oblige à la prise en compte encore plus effective";
//"------------------------------------------"

const char texte_33[] PROGMEM =  
  "interpelle le citoyen que je suis et nous oblige tous à aller de l'avant dans...";
//"------------------------------------------" 

const char texte_34[] PROGMEM = 
  "a pour conséquence obligatoire l'urgente nécessité";
//"------------------------------------------"

const char texte_35[] PROGMEM = 
  ">conforte mon désir incontestable d'aller dans le sens";
//"------------------------------------------"

const char texte_36[] PROGMEM = 
  "doit nous amener au choix réellement impératif";
//"------------------------------------------"

const char texte_37[] PROGMEM =  
  "doit prendre en compte les préoccupations de la population de base dans...";
//"------------------------------------------" 

const char texte_38[] PROGMEM = 
  "entraîne une mission somme toute des plus exaltantes pour moi : l'élaboration";
//"------------------------------------------"

const char* const section3[] PROGMEM = {texte_31, 
                                      texte_32, 
                                      texte_33, 
                                      texte_34, 
                                      texte_35,
                                      texte_36, 
                                      texte_37, 
                                      texte_38};

// SECTION 4
const char texte_41[] PROGMEM = 
  "d'un processus allant vers plus d'égalité.";
//"------------------------------------------"

const char texte_42[] PROGMEM = 
  "d'un avenir s'orientant vers plus de progrès et plus de justice.";
//"------------------------------------------"

const char texte_43[] PROGMEM =  
  "d'une restructuration dans laquelle chacun pourra enfin retrouver sa dignité.";
//"------------------------------------------" 

const char texte_44[] PROGMEM = 
  "d'une valorisation sans concession de nos caractères spécifiques.";
//"------------------------------------------"

const char texte_45[] PROGMEM = 
  "d'un plan correspondant véritablement aux exigences légitimes de chacun.";
//"------------------------------------------"

const char texte_46[] PROGMEM = 
  "de solutions rapides correspondant aux grands axes sociaux prioritaires.";
//"------------------------------------------"

const char texte_47[] PROGMEM =  
  "d'un programme plus humain, plus fraternel et plus juste.";
//"------------------------------------------" 

const char texte_48[] PROGMEM = 
  "d'un projet porteur de véritables espoirs, notamment pour les plus démunis.";
//"------------------------------------------"

const char* const section4[] PROGMEM = {texte_41, 
                                      texte_42, 
                                      texte_43, 
                                      texte_44, 
                                      texte_45,
                                      texte_46, 
                                      texte_47, 
                                      texte_48};

char buffer[3000];



// parallel port pin# = arduino pin#
const int nStrobe = 2;
const int data_0 = 3;
const int data_1 = 4;
const int data_2 = 5;
const int data_3 = 6;
const int data_4 = 7;
const int data_5 = 8;
const int data_6 = 9;
const int data_7 = 10;
const int nAck = 11;
const int busy = 12;

const int strobeWait = 200;   // microseconds to strobe for



// ******************************************
// *** INITIALISATION DE LA CARTE ARDUINO ***
// ******************************************
void setup() {
  Serial.begin(9600);

  pinMode(nStrobe, OUTPUT);      // is active LOW
  digitalWrite(nStrobe, HIGH);   // set HIGH
  pinMode(data_0, OUTPUT);
  pinMode(data_1, OUTPUT);
  pinMode(data_2, OUTPUT);
  pinMode(data_3, OUTPUT);
  pinMode(data_4, OUTPUT);
  pinMode(data_5, OUTPUT);
  pinMode(data_6, OUTPUT);
  pinMode(data_7, OUTPUT);

  pinMode(nAck, INPUT);     // is active LOW
  pinMode(busy, INPUT); 

  pinMode(13, OUTPUT);
  pinMode(14, INPUT);   // analog pin 0 on duemilanove and uno
  digitalWrite(14, HIGH); // enable pull-up
 
  delay(2000);
}



// **************************************
// *** BOUCLE PRINCIPALE DU PROGRAMME ***
// **************************************
void loop() {
  
  // Valable pour un bouton avec deux positions T
  //etatBouton = digitalRead(14);
  while(digitalRead(14) == HIGH) {
    delay(100);
    compteur++;
    if (compteur > 16000) compteur = 0;
  }
 
  printMessage(0);  
  for (int i=0 ; i<3 ; i++) {printByte(10);}  
  printMessage(1);  
  for (int i=0 ; i<7 ; i++) {printByte(10);}

  // Coupe le ticket
  printByte(27);
  printByte(109);
  
  delay(2000);
}



// *******************************************
// *** FONCTIONS APPELEES PAR LE PROGRAMME ***
// *******************************************


// Envoi d'un Octet passé en paramètre à l'Arduino
void printByte(byte inByte) {
  
  while(digitalRead(busy) == HIGH) {
    // wait for busy to go low
  }

  int b0 = bitRead(inByte, 0);
  int b1 = bitRead(inByte, 1);
  int b2 = bitRead(inByte, 2);
  int b3 = bitRead(inByte, 3);
  int b4 = bitRead(inByte, 4);
  int b5 = bitRead(inByte, 5);
  int b6 = bitRead(inByte, 6);
  int b7 = bitRead(inByte, 7);

  digitalWrite(data_0, b0);        // set data bit pins
  digitalWrite(data_1, b1);
  digitalWrite(data_2, b2);
  digitalWrite(data_3, b3);
  digitalWrite(data_4, b4);
  digitalWrite(data_5, b5);
  digitalWrite(data_6, b6);
  digitalWrite(data_7, b7);

  digitalWrite(nStrobe, LOW);       // strobe nStrobe to input data bits
  delayMicroseconds(strobeWait);
  digitalWrite(nStrobe, HIGH);

  while(digitalRead(busy) == HIGH) {
    // wait for busy line to go low
  }
}


// Imprimer un message
void printMessage(int num) {
  digitalWrite(13, HIGH);
 
  int nombreAleatoire1 = (compteur+random(8))%8; 
  int nombreAleatoire2 = (compteur+random(12))%8; 
  int nombreAleatoire3 = (compteur+random(16))%8; 
  int nombreAleatoire4 = (compteur+random(20))%8; 
  
   
  int correctCharacter = 0;
  String message = "";

  // Sélection du tableau à traiter (entête ou texte aléatoire)
  if (num == 0) {message = entete;}
  else {
    strcpy_P(buffer, (char*)pgm_read_word(&(section1[nombreAleatoire1])));
    message = message + " " + buffer;
    strcpy_P(buffer, (char*)pgm_read_word(&(section2[nombreAleatoire2])));
    message = message + " " + buffer;
    strcpy_P(buffer, (char*)pgm_read_word(&(section3[nombreAleatoire3])));
    message = message + " " + buffer;
    strcpy_P(buffer, (char*)pgm_read_word(&(section4[nombreAleatoire4])));
    message = message + " " + buffer;
  }
  
  for (int i = 0; i < message.length() ; i++) {
    
    byte character = utf8ascii(message.charAt(i));

    // Faute de mieux, on fait correspondre un à un les caractères qui s'affichent mal par le bon caractère :/
    if (character == 181) {correctCharacter = 230;} // µ
    else if (character == 212) {correctCharacter = 79;} // Ô -> O
    else if (character == 224) {correctCharacter = 133;} // à 
    else if (character == 226) {correctCharacter = 131;} // 
    else if (character == 228) {correctCharacter = 132;} //
    else if (character == 231) {correctCharacter = 135;} // ç
    else if (character == 232) {correctCharacter = 138;} // è
    else if (character == 233) {correctCharacter = 130;} // é
    else if (character == 234) {correctCharacter = 136;} // 
    else if (character == 235) {correctCharacter = 137;} // ë
    else if (character == 238) {correctCharacter = 140;} //
    else if (character == 239) {correctCharacter = 139;} // ï
    else if (character == 244) {correctCharacter = 147;} //
    else if (character == 246) {correctCharacter = 148;} //
    else if (character == 249) {correctCharacter = 151;} // ù
    else if (character == 250) {correctCharacter = 163;} //
    else if (character == 251) {correctCharacter = 150;} //
    else if (character == 252) {correctCharacter = 129;} //
    else {correctCharacter = character;}
    
    printByte(correctCharacter);
    
  }
  
  digitalWrite(13,LOW);
}



// ****** UTF8-Decoder: convert UTF8-string to extended ASCII *******
static byte c1;  // Last character buffer

// Convert a single Character from UTF8 to Extended ASCII
// Return "0" if a byte has to be ignored
byte utf8ascii(byte ascii) {
    if ( ascii<128 )   // Standard ASCII-set 0..0x7F handling  
    {   c1=0;
        return( ascii );
    }

    // get previous input
    byte last = c1;   // get last char
    c1=ascii;         // remember actual character

    switch (last)     // conversion depending on first UTF8-character
    {   case 0xC2: return  (ascii);  break;
        case 0xC3: return  (ascii | 0xC0); break;
        case 0x82: if(ascii==0xAC) return(0x80);       // special case Euro-symbol
    }

    return  (0);                                     // otherwise: return zero, if character has to be ignored
}

// convert String object from UTF8 String to Extended ASCII
String utf8ascii(String s)
{      
        String r="";
        char c;
        for (int i=0; i<s.length(); i++)
        {
                c = utf8ascii(s.charAt(i));
                if (c!=0) r+=c;
        }
        return r;
}

// In Place conversion UTF8-string to Extended ASCII (ASCII is shorter!)
void utf8ascii(char* s)
{      
        int k=0;
        char c;
        for (int i=0; i<strlen(s); i++)
        {
                c = utf8ascii(s[i]);
                if (c!=0)
                        s[k++]=c;
        }
        s[k]=0;
}
