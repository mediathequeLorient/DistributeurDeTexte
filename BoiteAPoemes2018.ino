/* 
Copyright (C) 2018 Frédéric Micout / Médiathèque de Lorient

  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) toute version ultérieure.
Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de détails.
Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

 */

#include <avr/pgmspace.h>
#include <EEPROM.h>


// Commentaire
boolean etatBouton = LOW;
int compteur = 0;

// Texte à afficher systématiquement en entête 
const char entete[] = 
  "Les médiathèques de Lorient vous offrent\n"
  "cette citation.";
//"------------------------------------------";


// Texte à afficher systématiquement en pied de page 
const char pieddepage[] = 
  "xxxxxxxxxx  mediatheque.lorient.fr\n"
  "xxxxxxxxxx  facebook\n"
  "xxx xx xxx  www.jenesaispasquoilire.net\n"
  "xxx xx xxx  02 97 84 33 60   \n"
  "xxx    xxx  4 place François Mitterrand  ";
//"------------------------------------------";



// Textes à afficher
const char texte_1[] PROGMEM =
  "On ne sait jamais comme on pense\n"
  "quand on y pense\n"
  "c'est à crever\n"
  "Penser qu'on pense :\n"
  "infernalise\n"
  "\n"
  "Penser,\n"
  "c'est d'abord la chair\n"
  "Penser langager corps\n"
  "et pas savoir comment l'accès :\n"
  "ça fait BOUKAN\n"
  "\n"
  "Penser ça rebondit\n"
  "pas plus loin que la chair\n"
  "Penser :\n"
  "c'est corps qui déboule\n"
  "Le chaos c'est d'abord tout moi\n"
  "\n"
  "Penser la pensée c'est terrible :\n"
  "c'est l'infernal du corps en vie\n"
  "\n"
  "\n"
  "Edith Azam\n"
  "Copyright : Atelier de l'agneau \n"
  "\n";

//"------------------------------------------";


const char texte_2[] PROGMEM =
  "Ô, solitude, l'île et ce volcan durci\n"
  " sous mes ongles\n"
  "La racine du dire, la vocable d'une\n"
  " éternité, partance\n"
  "Et soleils, si la joie se mesure à la\n"
  " peine, son regard\n"
  "Sur l'aire lourde des fanes couchées,\n"
  " pluie encore\n"
  "Pluie déliant l'obscur, dépliant les\n"
  " herbes, la souche\n"
  "D'une parole qui s'évade et prend le\n"
  " large, pieds\n"
  "Dans la boue et que roule un torrent,\n"
  " sa cascade\n"
  "Fontaine sous mes doigts, le feu, l'eau\n"
  " liés ensemble\n"
  "Pour façonner la glaise de ce corps\n"
  " éperdu et\n"
  "Qui tremble. Qui suis-je donc ?\n"
  " L'univers dévorant\n"
  "La plainte de son espace chargé et\n"
  " scories et lumières\n"
  "Que je tresse, baguette de sorcier dans\n"
  " les mains\n"
  "\n"
  "\n"
  "Jeanine BAude\n"
  "Poème extrait de Oui\n"
  "Copyright : La Rumeur Libre\n"
  "\n";

//"------------------------------------------";


const char texte_3[] PROGMEM =
  "Avant que la mer ne crache\n"
  "sa vague de fond\n"
  "avant que la tornade\n"
  "ne détruise maisons villes et villages\n"
  "ne divise le monde\n"
  "\n"
  "Le désir était déjà\n"
  "foudre sans nom\n"
  "\n"
  "\n"
  "Claudine Bertrand, Rouge assoiffée\n"
  "Copyright : éditions Typo \n"
  "\n";

//"------------------------------------------";

const char texte_4[] PROGMEM =
  "et je nage\n"
  "dans cette eau d'avant tous les ciels\n"
  "en haute et douce écume\n"
  "et je nage\n"
  "là où tous les deltas\n"
  "commencent\n"
  "à remonter vers leurs sources\n"
  "et je nage dans cette eau si eau\n"
  "qu'elle en devient\n"
  "rêve liquide\n"
  "offrande de silence\n"
  "mille siècles de vie\n"
  "\n"
  "\n"
  "Zéno Bianu\n"
  "Copyright : éditions Le Castor Astral\n"
  "\n";

//"------------------------------------------";

const char texte_5[] PROGMEM =
  "Il pleut des cordes\n"
  "\n"
  "j'en profite\n"
  "\n"
  "pour grimper\n"
  "\n"
  "au septième ciel\n"
  "\n"
  "\n"
  "Noème inédit \n"
  "Alain Borer\n"
  "\n";

//"------------------------------------------";

const char texte_6[] PROGMEM =
  "Mais quelle ardeur soudain s'est\n"
  " emparée de nous,\n"
  "ajoutant à la vitesse l'ombre filiforme\n"
  " des clôtures ?\n"
  "Quand la pisse d'une vache cambrée jette\n"
  "l'arc-en-ciel aux nues.\n"
  " Un nuage passe – le soleil.\n"
  "\n"
  "\n"
  "Pascal Commère\n"
  "Poème extrait de Vessies, lanternes,\n"
  " autres bêtes cornues \n"
  "Copyright : éditions Obsidiane\n"
  "\n";

//"------------------------------------------";

const char texte_7[] PROGMEM =
  "la nage peut se mesurer à la longueur\n"
  " du corps\n"
  "mais\n"
  "je nage sans voir\n"
  "\n"
  "le ciel\n"
  "empli\n"
  "\n"
  "écrivant sur des papiers qui auraient\n"
  " pu être teintés de bleu\n"
  "de grandes éclaircies\n"
  "(ouvrent sur les jours\n"
  "ouvrent sur les rues\n"
  "à nouveau le corps nage\n"
  "sans voir\n"
  "\n"
  "d'un revers de main\n"
  "\n"
  "devant\n"
  "\n"
  "le poids\n"
  "me donne\n"
  "corps\n"
  "\n"
  "les\n"
  "poussières\n"
  "sur le côté\n"
  "\n"
  "\n"
  "Fabienne Courtade\n"
  "Poèmes extraits de Le même geste\n"
  "Copyright : éditions Flammarion\n"
  "\n";

//"------------------------------------------";

const char texte_8[] PROGMEM =
  "Les miroirs ?\n"
  "On les traversera.\n"
  "\n"
  "Dans la nuit des poèmes. Ou celle des\n"
  " images.\n"
  "Quand l'œil fend les paupières et la\n"
  " langue les secrets.\n"
  "Vers quel jour ?\n"
  "\n"
  "Un jour de nom mortel dont on ne sait\n"
  " rien que cette saveur de terre.\n"
  "\n"
  "Un jour de grand soleil, d'orties\n"
  " orageuses et de roses muettes. \n"
  "Un jour à jeter l'épervier sur nos eaux\n"
  " périssables, \n"
  "nos rivières aux présents menacés, \n"
  "pour quelques poignées de ciel.\n"
  "\n"
  "Et libre rivière, passer !\n"
  "\n"
  "\n"
  "Alain Freixe\n"
  "Copyright : éditions L'Amourier\n"
  "\n";

//"------------------------------------------";

const char texte_9[] PROGMEM =
  "Je dédie mes poèmes\n"
  "à tout ce que je ne comprends pas\n"
  "\n"
  "À tout ce qui existe\n"
  "et que je ne vois pas\n"
  "\n"
  "Je les dédie au silence\n"
  "qui se trouve au fond\n"
  "de chaque fracas\n"
  "\n"
  "\n"
  "Anise Koltz\n"
  "Poème extrait de Galaxies intérieures\n"
  "Copyright : éditions Arfuyen\n"
  "\n";

//"------------------------------------------";

const char texte_10[] PROGMEM =
  "   presque rien, juste une brûlure\n"
  "on peut toujours\n"
  "ne pas remettre à plus tard\n"
  "\n"
  "tu le sais\n"
  "c'est ce que finalement\n"
  "tu fais\n"
  "\n"
  "tu te lèves un matin à cinq heures\n"
  "regardes l'aube\n"
  "depuis une barque au milieu de l'étang\n"
  "\n"
  "la queue d'un poisson frappe\n"
  "la surface\n"
  "des animaux viennent boire\n"
  "\n"
  "cette aube\n"
  "d'elle aussi, tu dis\n"
  "elle ne reviendra pas\n"
  "\n"
  "mais l'instant\n"
  "ne tend pas\n"
  "la perche au poème\n"
  "\n"
  "\n"
  "Camille Loivier\n"
  "Copyright : éditions Tarabuste\n"
  "\n";

//"------------------------------------------";

const char texte_11[] PROGMEM =
  "Trinquant à la santé du bleu\n"
  "Rouges appels aux véhémences\n"
  "De quels feux êtes-vous semences\n"
  "Pivoines nées coiffées des dieux\n"
  "\n"
  "On dit que les printemps sont doux\n"
  "Je dis moi – et je suis poète –\n"
  "Qu'ils sont un peu comme vous êtes\n"
  "Excédés farfelus et fous\n"
  "\n"
  "Mais qu'elle est noble leur folie\n"
  "Grandiose votre douleur\n"
  "Tragédiennes entre les fleurs\n"
  "– Il faut mourir – Aimons la vie\n"
  "\n"
  "\n"
  "Maximine\n"
  "Copyright : éditions Arfuyen \n"
  "\n";

//"------------------------------------------";

const char texte_12[] PROGMEM =
  "Eric ? un roc ! foc au récif rac ric\n"
  "Entre les grains au plus près le bec pique\n"
  "De piaf ou mésange à tête noire hic\n"
  "Traduit sec en (Breizh atao !) Pen Duick !\n"
  "\n"
  "Mutique (ou constipé ?) l'captain en poupe\n"
  "Crispe un maxillaire amer sur des soupes\n"
  "D'embruns vers les îles à rhum ou la\n"
  "Samba furibarde du Horn écla\n"
  "\n"
  "B'houssé de plume. Après l'écume, aux\n"
  "Non d'ours ou loups pas plus que lui\n"
  " causants,\n"
  "\n"
  "Elle (Erin, Odet : Arcadie !) attend\n"
  "Amoureuse à mort, la Manche - et adieu !\n"
  "\n"
  "\n"
  "Christian Prigent\n"
  "Poème extrait de Chino aime le sport\n"
  "Copyright : éditions P.O.L\n"
  "\n";

//"------------------------------------------";

const char texte_13[] PROGMEM =
  "L'ange n'est pas un oiseau, mi-homme,\n"
  " mi-dieu, suspendu entre ciel et terre. \n"
  "Une créature mièvre pour rassurer les\n"
  " faibles. Un messager. Un médium. Non.\n"
  "L'ange apparaît dès que les mots\n"
  " se retirent.\n"
  "Et qu'un accord tacite clôt les regards\n"
  " sur un vide clair, sans refus. \n"
  "Une stupeur bienveillante. \n"
  "L'ange est un silence qui s'accomplit\n"
  " alors que personne ne l'attend. \n"
  "Une vague qui remplit tout, effaçant\n"
  " les peurs, les désirs.\n"
  "Une accalmie souveraine remerciée comme\n"
  " un miracle.\n"
  "\n"
  "\n"
  "Dominique Sampiero, La vie est chaude\n"
  "Copyright : éditions Bruno Doucey \n"
  "\n";

//"------------------------------------------";

const char texte_14[] PROGMEM =
  "Le géant court sur la steppe, il court,\n"
  " courant il pense\n"
  "à sa fiancée perdue, courir pense, ne\n"
  " pense qu'à elle perdue,\n"
  "Que pense la steppe sous ses pas leur\n"
  " fureur\n"
  "est terrible, que pense-t-elle de la\n"
  " pensée du géant qui court\n"
  "et de la fiancée, il la tenait dans\n"
  " sa main,\n"
  "la montait jusqu'au bord des yeux,\n"
  " qu'elle parle mais qu'elle\n"
  "parle en se tortillant sur ses doigts,\n"
  " assise là se tortillant\n"
  "elle lui disait de jolies choses\n"
  "qu'il ne comprenait pas, il était\n"
  "heureux\n"
  "heureux\n"
  "heureux à cette époque\n"
  "\n"
  "\n"
  "Je veux joie, je veux joie,\n"
  " je veux joie.\n"
  "\n"
  "Hélène Sanguinetti\n"
  "Copyright : éditions La Lettre volée\n"
  "édition printemps des poetes 2018 \n"
  "\n";

//"------------------------------------------";


const char texte_15[] PROGMEM =
  "Elle\n"
  "noire de la jupe aux cheveux\n"
  "des yeux jusqu'au corsage\n"
  "bouche mince\n"
  "et rouge comme noire\n"
  "je me rappelle son pas\n"
  "glissando\n"
  "une brise retenue\n"
  "un tigre insomniaque et fiévreux\n"
  "le nom d'une fleur mauve\n"
  "quand je remontais du port\n"
  "Goûter sa salive\n"
  "\n"
  "\n"
  "Eric Sarner, Cœur Chronique\n"
  "Copyright : éditions Le Castor Astral\n"
  "\n";

//"------------------------------------------";
 
const char texte_16[] PROGMEM =
  "Vivant, toi, salut !\n"
  "Nous sommes vivants.\n"
  "Toi l'adorable à la vie brève et à\n"
  " la vie tout court,\n"
  "salut ! Salut !\n"
  "\n"
  "Vers quoi nous levons-nous ?\n"
  "\n"
  "Ta salive délivre les sourds-muets\n"
  "et laisse dans la bouche\n"
  "une envie de rire pour toujours.\n"
  "\n"
  "D'où vient ce matin de décembre ?\n"
  "\n"
  "Bienvenu dans la chair,\n"
  "toi son hôte candide, et sauve-nous !\n"
  "\n"
  "\n"
  "Ariel Spiegler\n"
  "Copyright : éditions Corvelour –\n"
  " Revue Nunc\n"
  "\n";

//"------------------------------------------";

const char texte_17[] PROGMEM =
  "Nous irons à Valparaiso !\n"
  "Nous irons à Saint-Affrique par\n"
  " la ruisseau\n"
  "départemental numéro 18 !\n"
  "Nous irons à Chandernagor et à\n"
  " Petit Shériff !\n"
  "Nous irons aux Cascades de Coo,\n"
  " en Belgique !\n"
  "Nous irons aux cascades de Coït !\n"
  "\n"
  "Nous remonterons Cahuètte et l'Escaut,\n"
  " cher\n"
  "au courrier du cœur des lecteurs\n"
  " du Courrier\n"
  "de l'Escaut !\n"
  "Nous suivrons la Méthode Ogino en\n"
  " suivant la\n"
  "Direction Centre-ville !\n"
  "Nous suivrons la Métèque à Mimile\n"
  " et la\n"
  "Méthode Haschischmil !\n"
  "Nous ferons Tôôt et Tûtûte,\n"
  " s'il le faut !\n"
  "Nous irons : où vas-tu Basile !\n"
  "Nous irons à Vaduz sur la tombe de\n"
  " Buffalo Bill !\n"
  "Nous irons voir l'Eau, un marbre de\n"
  " Legros !\n"
  "Nous irons à Contrex-Séville, voir les\n"
  " Madame Bovaril\n"
  "ou danser le flamenco des cils,\n"
  "dans les Yvelines, à Les Mureaux !\n"
  "Nous irons ici !\n"
  "Nous irons à Paris !\n"
  "Nous irons à pédalo !\n"
  "\n"
  "Nous irons à Valparéo ! (...)\n"
  "\n"
  "\n"
  "Jean-Pierre Verheggen\n"
  "Poème extrait de Ridiculum vitae,\n"
  "Copyright : La Différence \n"
  "\n";

//"------------------------------------------";

const char texte_18[] PROGMEM =
  "les paroles empoisonnent la vie\n"
  "les paroles il y a ce truc de pas clair\n"
  "qui grouille en dedans de la vie\n"
  "les paroles le truc de pas clair\n"
  "fait tout de suite des étincelles dans\n"
  " la vie\n"
  "les paroles tordent la vie\n"
  "jusqu'au point où c'est plus respirable\n"
  "les paroles font comme un plâtre\n"
  "et du coup la vie étouffe\n"
  "il faut reprendre la vie\n"
  "il faut repasser les paroles\n"
  "par l'écrit\n"
  "il faut reprendre les paroles et\n"
  " les tordre\n"
  "dans l'écrit\n"
  "l'écrit est la mort des paroles\n"
  "mais l'écrit ne tue pas la vie\n"
  "la vie progresse\n"
  "la vie sort et chante et roule dans\n"
  " l'air\n"
  "la vie donne de l'air au sens\n"
  "l'air varie, il fluctue\n"
  "il épaissit mais s'éparpille aussi\n"
  "la vie est une chance de parler\n"
  "hors des paroles\n"
  "la vie est un chant dans l'écrit\n"
  "\n"
  "\n"
  "Charles Pennequin\n"
  "Poème extrait de Pamphlet contre la mort\n"
  "Copyright : P.O.L\n"
  "\n";

//"------------------------------------------";

const char texte_19[] PROGMEM =
  "Bras nus décolleté peau brunie\n"
  "\n"
  "Soleil\n"
  "\n"
  "La nappe dépliée le dimanche\n"
  "Les cloches au village ponctuent\n"
  " le temps\n"
  "Du monde sur le parvis des visages\n"
  " connus\n"
  "\n"
  "Toujours on répond ça va merci et toi ?\n"
  "\n"
  "\n"
  "En fin de course la lumière cède\n"
  "Un interruption anonyme\n"
  "Peinture écailleé de l'arrière-pays\n"
  "\n"
  "\n"
  "Mireille Fargier-Caruso\n"
  "Poème extrait de Un lent dépaysage\n"
  "Copyright : éditions Bruno Doucey\n"
  "\n";

//"------------------------------------------";

const char texte_20[] PROGMEM =
  "Ce soir \n"
  "Tandis que ce poème tente de s'inscrire\n"
  " au revers de l'image\n"
  "je flotte dans l'apesanteur\n"
  " des anesthésies\n"
  "entre un ciel sans lendemains\n"
  "et la terre qui n'oublie rien\n"
  "\n"
  "Quand le matin arrive enfin\n"
  "au-dessus de moi mes mains s'ouvrent\n"
  "\n"
  "Elles sont vides\n"
  "\n"
  "\n"
  "Marc Delouze\n"
  "Poème extrait de 14975 JOURS ENTRE\n"
  "Copyright : La passe du vent\n"
  "\n";

//"------------------------------------------";

const char texte_21[] PROGMEM =
  "Kind of fear, crainte de perdre ou\n"
  " de manquer, nar omverdenen\n"
  "er optaget af det materielle, and I walk\n"
  " around unden mal and realise \n"
  "that many of my good friends are\n"
  " wretched,\n"
  "that by now I am poorly spoken \n"
  "and my experience of life is not\n"
  " satisfied with ferocity and fine words.\n"
  "At the end of the day, les mots\n"
  " se téléscopent \n"
  "et il en ressort un sens subtil qui\n"
  " me laisse insatisfait,\n"
  "étrangement perdu, car les réalités\n"
  " écrites sont plus lourdes que cela.\n"
  "\n"
  "\n"
  "Paul de  Brancion\n"
  "Poème extrait de Ma Mor est morte\n"
  "Copyright : éditions Bruno Doucey\n"
  "\n";

//"------------------------------------------";

const char texte_22[] PROGMEM =
  "Les vagues ne roulent plus\n"
  "elles rythment juste le temps\n"
  "d'une admiration lente\n"
  "\n"
  "Sur le sable je ramasse\n"
  "les lettres éparpillées\n"
  "qui formaient la victoire\n"
  "\n"
  "Savourant chaque pas\n"
  "jusqu'à ce que s'effrite\n"
  "la peur qui m'habitait\n"
  "\n"
  "Plus personne désormais \n"
  "ne pourra me contraindre.\n"
  "\n"
  "\n"
  "Stéphane Bataillon\n"
  "Poème extrait de Où nos ombres s'épousent\n"
  "Copyright : éditions Bruno Doucey\n"
  "\n";

//"------------------------------------------";

const char texte_23[] PROGMEM =
  "Les mots sont les vêtements de l'émotion\n"
  "Et même si nos stylos habillent bien nos\n"
  " phrases\n"
  "Peuvent-ils vraiment sauver nos frères\n"
  " du naufrage ?\n"
  "Les poètes se cachent pour écrire\n"
  "C'est pas une légende Rouda regarde\n"
  " nous\n"
  "On a traversé des rivières de boue\n"
  " à la nage\n"
  "On a dormi à jeun dans la neige et\n"
  " on est encore debout\n"
  "Les poètes se cachent pour écrire\n"
  "Chacun purge sa pénombre\n"
  "\n"
  "\n"
  "Souleymane Diamanka\n"
  "Chanson extraite de l'album L'hiver peul\n"
  "Copyright : Universal Music\n"
  "\n";

//"------------------------------------------";




int nbTextes = 23;

const char* const textes[] PROGMEM = {texte_1,texte_2,texte_3,texte_4,texte_5,texte_6,texte_7,texte_8,texte_9,texte_10,texte_11,texte_12,texte_13,texte_14,texte_15,texte_16,texte_17,texte_18,texte_19,texte_20,texte_21,texte_22,texte_23};

char buffer[2000];



// parallel port pin# = arduino pin#
const int nStrobe = 2;
const int data_0 = 3;
const int data_1 = 4;
const int data_2 = 5;
const int data_3 = 6;
const int data_4 = 7;
const int data_5 = 8;
const int data_6 = 9;
const int data_7 = 10;
const int nAck = 11;
const int busy = 12;

const int strobeWait = 200;   // microseconds to strobe for



// ******************************************
// *** INITIALISATION DE LA CARTE ARDUINO ***
// ******************************************
void setup() {

  // Décommenter ce qui suit et téléverser pour initialiser le compteur
  // Ne pas oublier de le recommenter ensuite et de re-téléverser de nouveau le soft
  /*int compteurImpression = 0;
  EEPROM.put(0,compteurImpression);*/
  
  Serial.begin(9600);

  pinMode(nStrobe, OUTPUT);      // is active LOW
  digitalWrite(nStrobe, HIGH);   // set HIGH
  pinMode(data_0, OUTPUT);
  pinMode(data_1, OUTPUT);
  pinMode(data_2, OUTPUT);
  pinMode(data_3, OUTPUT);
  pinMode(data_4, OUTPUT);
  pinMode(data_5, OUTPUT);
  pinMode(data_6, OUTPUT);
  pinMode(data_7, OUTPUT);

  pinMode(nAck, INPUT);     // is active LOW
  pinMode(busy, INPUT); 

  pinMode(13, OUTPUT);
  pinMode(14, INPUT);   // analog pin 0 on duemilanove and uno
  digitalWrite(14, HIGH); // enable pull-up
 
  delay(2000);
}



// **************************************
// *** BOUCLE PRINCIPALE DU PROGRAMME ***
// **************************************
void loop() {
  
  // Valable pour un bouton avec une positions T
  etatBouton = HIGH;
  while(digitalRead(14) == HIGH) {
    delay(100);
    compteur++;
    if (compteur > 16000) compteur = 0;
  }

  // Appel de la fonction mémorisant le nombre d'impression
  compteurImpressions(); 
  
  // Choix des blocs à imprimer
  /*printMessage(0);  
  for (int i=0 ; i<4 ; i++) {printByte(10);}  */
  printMessage(1);   
  for (int i=0 ; i<4 ; i++) {printByte(10);}  
  printMessage(2);  
  for (int i=0 ; i<7 ; i++) {printByte(10);}
  
  // Coupe le ticket
  printByte(27);
  printByte(109);
  
  delay(2000);

  // Dans le cas où le bouton est maintenu
  if(digitalRead(14) == LOW) {
    printMessage(3);
    for (int i=0 ; i<7 ; i++) {printByte(10);}
    // Coupe le ticket
    printByte(27);
    printByte(109);
  }
  
  while(digitalRead(14) == LOW) {
    delay(1);
  }
}



// *******************************************
// *** FONCTIONS APPELEES PAR LE PROGRAMME ***
// *******************************************


// Envoi d'un Octet passé en paramètre à l'Arduino
void printByte(byte inByte) {
  
  while(digitalRead(busy) == HIGH) {
    // wait for busy to go low
  }

  int b0 = bitRead(inByte, 0);
  int b1 = bitRead(inByte, 1);
  int b2 = bitRead(inByte, 2);
  int b3 = bitRead(inByte, 3);
  int b4 = bitRead(inByte, 4);
  int b5 = bitRead(inByte, 5);
  int b6 = bitRead(inByte, 6);
  int b7 = bitRead(inByte, 7);

  digitalWrite(data_0, b0);        // set data bit pins
  digitalWrite(data_1, b1);
  digitalWrite(data_2, b2);
  digitalWrite(data_3, b3);
  digitalWrite(data_4, b4);
  digitalWrite(data_5, b5);
  digitalWrite(data_6, b6);
  digitalWrite(data_7, b7);

  digitalWrite(nStrobe, LOW);       // strobe nStrobe to input data bits
  delayMicroseconds(strobeWait);
  digitalWrite(nStrobe, HIGH);

  while(digitalRead(busy) == HIGH) {
    // wait for busy line to go low
  }
}


// Compteur d'impression depuis le dernier chargement du soft
void compteurImpressions() {
  
  int compteurImpression;
  compteurImpression = EEPROM.read (0);
  compteurImpression++;
  EEPROM.put(0,compteurImpression);
  
}

// Imprimer un message
void printMessage(int num) {
  digitalWrite(13, HIGH);
 
  int nombreAleatoire = compteur%nbTextes;  
  int correctCharacter = 0;
  String message;

  //for (int j = 0; j < 23 ; j++) {

  // Sélection du tableau à traiter (entête ou texte aléatoire)
  if (num == 0) {message = entete;}
  else if (num == 2) {message = pieddepage;}
  else if (num == 3) {message = EEPROM.read (0);}
  else {
    strcpy_P(buffer, (char*)pgm_read_word(&(textes[nombreAleatoire])));
    //strcpy_P(buffer, (char*)pgm_read_word(&(textes[j])));
    message = buffer;
  }
  
  for (int i = 0; i < message.length() ; i++) {
    
    byte character = utf8ascii(message.charAt(i));

    // Faute de mieux, on fait correspondre un à un les caractères qui s'affichent mal par le bon caractère :/
    if (character == 233) {correctCharacter = 130;} // é
    else if (character == 232) {correctCharacter = 138;} // è
    else if (character == 234) {correctCharacter = 136;} // 
    else if (character == 231) {correctCharacter = 135;} // ç
    else if (character == 238) {correctCharacter = 140;} //
    else if (character == 239) {correctCharacter = 139;} //
    else if (character == 224) {correctCharacter = 133;} // à 
    else if (character == 226) {correctCharacter = 131;} // 
    else if (character == 228) {correctCharacter = 132;} //
    else if (character == 244) {correctCharacter = 147;} //
    else if (character == 246) {correctCharacter = 148;} //
    else if (character == 249) {correctCharacter = 151;} // ù
    else if (character == 250) {correctCharacter = 163;} //
    else if (character == 251) {correctCharacter = 150;} //
    else if (character == 252) {correctCharacter = 129;} //
    else if (character == 181) {correctCharacter = 230;} // µ
    else if (character == 212) {correctCharacter = 79;} // Ô -> O
    else {correctCharacter = character;}
    
    printByte(correctCharacter);
    
  }

  //}
  
  digitalWrite(13,LOW);
}



// ****** UTF8-Decoder: convert UTF8-string to extended ASCII *******
static byte c1;  // Last character buffer

// Convert a single Character from UTF8 to Extended ASCII
// Return "0" if a byte has to be ignored
byte utf8ascii(byte ascii) {
    if ( ascii<128 )   // Standard ASCII-set 0..0x7F handling  
    {   c1=0;
        return( ascii );
    }

    // get previous input
    byte last = c1;   // get last char
    c1=ascii;         // remember actual character

    switch (last)     // conversion depending on first UTF8-character
    {   case 0xC2: return  (ascii);  break;
        case 0xC3: return  (ascii | 0xC0); break;
        case 0x82: if(ascii==0xAC) return(0x80);       // special case Euro-symbol
    }

    return  (0);                                     // otherwise: return zero, if character has to be ignored
}

// convert String object from UTF8 String to Extended ASCII
String utf8ascii(String s)
{      
        String r="";
        char c;
        for (int i=0; i<s.length(); i++)
        {
                c = utf8ascii(s.charAt(i));
                if (c!=0) r+=c;
        }
        return r;
}

// In Place conversion UTF8-string to Extended ASCII (ASCII is shorter!)
void utf8ascii(char* s)
{      
        int k=0;
        char c;
        for (int i=0; i<strlen(s); i++)
        {
                c = utf8ascii(s[i]);
                if (c!=0)
                        s[k++]=c;
        }
        s[k]=0;
}

